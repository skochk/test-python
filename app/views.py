from flask import render_template
from app import app

@app.route('/')

@app.route('/index')
def index():
    user = { 'nickname': 'Miguel' } # выдуманный пользователь
    userList = [{'nickname':'sasha'}, {'nickname':'Boris'}] # test data
    return render_template("index.html",
        title = 'Home',
        user = user,
        userList = userList)